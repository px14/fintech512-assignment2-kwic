import java.util.*;

class KwicIdxGenerator {

    static public HashSet<String> parseIgnoreWordsStr(String ignoreWordsStr){
        /* Return a set of to-be-ignored words given the first half of input string */
        return new HashSet<>(List.of(ignoreWordsStr.split("\n")));
    }

    static public ArrayList<String> wordsOfTitle(String title){
        /* Return a list of words given the title string */
        return new ArrayList<>(List.of(title.split(" ")));
    }

    static public ArrayList<ArrayList<String>> parseTitlesStr(String titlesStr){
        /* Return a list of word-lists given the second half of input string,
        each word-list corresponds to a title. All the words are in lowercase */
        ArrayList<ArrayList<String>> titles = new ArrayList<>();
        for (String title : titlesStr.toLowerCase().split("\n")){
            titles.add(wordsOfTitle(title));
        }
        return titles;
    }

    static public TreeMap<String, ArrayList<int[]>> getKeyWordPositions(ArrayList<ArrayList<String>> titles,
                                                                HashSet<String> ignoreWords){
        /*
        Return a tree map of keywords.
        keys: a key word,
        values: [{i, j}, ...] {i, j} records a keyword's position,
                means that the key word is at the i-th title's j-th word
         */
        TreeMap<String, ArrayList<int[]>> keyWords = new TreeMap<>();
        for (int i = 0; i < titles.size(); i++) {
            for (int j = 0; j < titles.get(i).size(); j++){
                String word = titles.get(i).get(j);

                // skip words to be ignored
                if (ignoreWords.contains(word)){
                    continue;
                }

                // record current position (i-th title, j-th word)
                if(keyWords.containsKey(word)){
                    keyWords.get(word).add(new int[]{i, j});
                }
                else{
                    keyWords.put(word, new ArrayList<>(List.of(new int[]{i, j})));
                }
            }
        }
        return keyWords;
    }

    static public String generateKwicIndex(TreeMap<String, ArrayList<int[]>> keyWordPositions,
                                            ArrayList<ArrayList<String>> titles){
        /* Generate the KwicIndex */
        StringBuilder kwicIndex = new StringBuilder();

        for (String key : keyWordPositions.keySet()){
            for (int[] position: keyWordPositions.get(key)) {

                // add the words in the title to kwicIndex
                ArrayList<String> title = titles.get(position[0]);
                for (int i = 0; i < title.size(); i++){

                    // if the i-th word is the key word, add the uppercase one
                    if (i == position[1]){
                        kwicIndex.append(key.toUpperCase());
                    }
                    else{
                        kwicIndex.append(title.get(i));
                    }

                    // add " " as delimiters between words
                    if (i != title.size() - 1){
                        kwicIndex.append(" ");
                    }
                }
                // add "\n" as delimiters between titles
                kwicIndex.append("\n");
            }
        }
        return kwicIndex.toString();
    }

    static public String generateKwicIndex(String input){
        String [] inputs = (" " + input + " ").split("::\n");
        if (inputs.length != 2){
            System.out.println("Too many separators!\n");
            throw new IllegalArgumentException();
        }

        if (!inputs[0].equals(inputs[0].toLowerCase())){
            System.out.println("The words to be ignored should be in lower case!\n");
            throw new IllegalArgumentException();
        }

        HashSet<String> ignoreWords = parseIgnoreWordsStr(inputs[0]);
        ArrayList<ArrayList<String>> titles = parseTitlesStr(inputs[1]);
        TreeMap<String, ArrayList<int[]>> kewWordPositions = getKeyWordPositions(titles, ignoreWords);

        return generateKwicIndex(kewWordPositions, titles);
    }
}
