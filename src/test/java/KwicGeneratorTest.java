
import java.util.*;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;


class KwicGeneratorTest {

    @Test
    void testParseIgnoredWords(){
        HashSet<String> result = KwicIdxGenerator.parseIgnoreWordsStr("the\nof\nand\nas\na\n");
        Set<String> ans = new HashSet<>(Arrays.asList("the", "and", "of", "as", "a"));
        assertEquals(ans, result);
    }

    @Test
    void testWordsOfTitle(){
        ArrayList<String> result = KwicIdxGenerator.wordsOfTitle("the old man and the sea");
        ArrayList<String> ans = new ArrayList<>(Arrays.asList("the", "old", "man", "and", "the", "sea"));
        assertEquals(ans, result);
    }

    @Test
    void testParseTitles(){
        String titlesStr = """
                            Descent of Man
                            The Ascent of Man
                            The Old Man and The Sea""";

        ArrayList<ArrayList<String>> result = KwicIdxGenerator.parseTitlesStr(titlesStr);

        ArrayList<ArrayList<String>> ans = new ArrayList<>();
        ans.add(new ArrayList<>(Arrays.asList("descent", "of", "man")));
        ans.add(new ArrayList<>(Arrays.asList("the", "ascent", "of", "man")));
        ans.add(new ArrayList<>(Arrays.asList("the", "old", "man", "and", "the", "sea")));

        assertEquals(ans, result);
    }

    @Test
    void testGetKeyWords(){

        HashSet<String> ignoreWords = new HashSet<>(Arrays.asList("the", "and", "of", "as", "a"));

        ArrayList<ArrayList<String>> wordLists = new ArrayList<>();
        wordLists.add(new ArrayList<>(Arrays.asList("descent", "of", "man")));
        wordLists.add(new ArrayList<>(Arrays.asList("the", "ascent", "of", "man")));
        wordLists.add(new ArrayList<>(Arrays.asList("the", "old", "man", "and", "the", "sea")));

        // actual
        TreeMap<String, ArrayList<int[]>> result = KwicIdxGenerator.getKeyWordPositions(wordLists, ignoreWords);

        // expected
        TreeMap<String, ArrayList<int[]>> ans = new TreeMap<>();
        ans.put("ascent", new ArrayList<>(List.of(new int[]{1, 1})));
        ans.put("descent", new ArrayList<>(List.of(new int[]{0, 0})));
        ans.put("man", new ArrayList<>(List.of(new int[]{0, 2}, new int[]{1, 3}, new int[]{2, 2})));
        ans.put("old", new ArrayList<>(List.of(new int[]{2, 1})));
        ans.put("sea", new ArrayList<>(List.of(new int[]{2, 5})));

        // compare two tree maps
        // can't use equal because int[] are compared by address but not values

        // test keys have the same order
        assertArrayEquals(ans.keySet().toArray(), result.keySet().toArray());

        // test values are equal
        for (String key: ans.keySet()){
            ArrayList<int[]> ans_positions = ans.get(key);
            ArrayList<int[]> result_positions = result.get(key);
            assertEquals(ans_positions.size(), result_positions.size());
            // test positions are equal
            for (int i = 0; i < ans_positions.size(); i++){
                assertArrayEquals(ans_positions.get(i), result_positions.get(i));
            }
        }
    }

    @Test
    void testGenerateKwicIndex0(){

        ArrayList<ArrayList<String>> wordLists = new ArrayList<>();
        wordLists.add(new ArrayList<>(Arrays.asList("descent", "of", "man")));
        wordLists.add(new ArrayList<>(Arrays.asList("the", "ascent", "of", "man")));
        wordLists.add(new ArrayList<>(Arrays.asList("the", "old", "man", "and", "the", "sea")));

        TreeMap<String, ArrayList<int[]>> keyWords = new TreeMap<>();
        keyWords.put("ascent", new ArrayList<>(List.of(new int[]{1, 1})));
        keyWords.put("descent", new ArrayList<>(List.of(new int[]{0, 0})));
        keyWords.put("man", new ArrayList<>(List.of(new int[]{0, 2}, new int[]{1, 3}, new int[]{2, 2})));
        keyWords.put("old", new ArrayList<>(List.of(new int[]{2, 1})));
        keyWords.put("sea", new ArrayList<>(List.of(new int[]{2, 5})));

        // actual
        String result = KwicIdxGenerator.generateKwicIndex(keyWords, wordLists);

        // expected
        String ans = """
                the ASCENT of man
                DESCENT of man
                descent of MAN
                the ascent of MAN
                the old MAN and the sea
                the OLD man and the sea
                the old man and the SEA
                """;
        assertEquals(ans, result);
    }


    @Test
    void testGenerateKwicIndex1(){
        String input = """
                        is
                        the
                        of
                        and
                        as
                        a
                        but
                        ::
                        Descent of Man
                        The Ascent of Man
                        The Old Man and The Sea
                        A Portrait of The Artist As a Young Man
                        A Man is a Man but Bubblesort IS A DOG
                        """;
        String output = """
                        a portrait of the ARTIST as a young man
                        the ASCENT of man
                        a man is a man but BUBBLESORT is a dog
                        DESCENT of man
                        a man is a man but bubblesort is a DOG
                        descent of MAN
                        the ascent of MAN
                        the old MAN and the sea
                        a portrait of the artist as a young MAN
                        a MAN is a man but bubblesort is a dog
                        a man is a MAN but bubblesort is a dog
                        the OLD man and the sea
                        a PORTRAIT of the artist as a young man
                        the old man and the SEA
                        a portrait of the artist as a YOUNG man
                        """;

        assertEquals(output, KwicIdxGenerator.generateKwicIndex(input));
    }

    @Test
    void testGenerateKwicIndex2(){
        String input = """
                        is
                        the
                        of
                        and
                        as
                        a
                        but
                        ::
                        """;
        String output = """
                        """;

        assertEquals(output, KwicIdxGenerator.generateKwicIndex(input));
    }

    @Test
    void testGenerateKwicIndex3(){
        String input = """
                        ::
                        the Descent of Man
                        Ascent of Man
                        """;
        String output = """
                        ASCENT of man
                        the DESCENT of man
                        the descent of MAN
                        ascent of MAN
                        the descent OF man
                        ascent OF man
                        THE descent of man
                        """;

        assertEquals(output, KwicIdxGenerator.generateKwicIndex(input));
    }

    @Test
    void testGenerateKwicIndex4(){
        String input = """
                        and
                        of
                        a
                        ::
                        an
                        ::
                        the Descent of Man
                        Ascent of Man
                        """;

        assertThrows(IllegalArgumentException.class, ()->KwicIdxGenerator.generateKwicIndex(input));
    }

    @Test
    void testGenerateKwicIndex5(){
        String input = """
                        and
                        Of
                        A
                        an
                        ::
                        the Descent of Man
                        Ascent of Man
                        """;

        assertThrows(IllegalArgumentException.class, ()-> KwicIdxGenerator.generateKwicIndex(input));
    }
}
